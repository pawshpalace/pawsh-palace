Our mission at Pawsh Palace is to provide outstanding pet care. To provide the highest level of pet care we utilize the most up to date pet care systems for our guests to enjoy an active day in a safe and clean environment.

Address: 3081 Business Ln, Las Vegas, NV 89103, USA

Phone: 702-931-1778

Website: http://www.pawshpalacelv.com
